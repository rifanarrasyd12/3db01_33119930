<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <!-- Bootstrap CSS -->
 <style>
body {background:powderblue; color:black; margin-left:0.5in; font-family:arial}
</style>
<title>UTS</title>
</head>
<body>
<table border="1" style="background:white; width:50%; margin: auto; padding: 10px; text-align: center;">
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>NPM</th>
			<th>Kelas</th>
            <th>Alamat</th>
		</tr>
		<tr>
			<td>1</td>
			<td>Andy Saputra</td>
			<td>3456756</td>
			<td>3DB04</td>
            <td>Jakarta</td>
		</tr>
		<tr>
			<td>2</td>
			<td>Calvin Semeru</td>
			<td>3634775</td>
			<td>3DB04</td>
            <td>Bogor</td>
		</tr>
		<tr>
			<td>3</td>
			<td>Dias Junaidi</td>
			<td>3225901</td>
			<td>3DB04</td>
            <td>Tangerang</td>
		</tr>
		<tr>
			<td>4</td>
			<td>Ratchel Amara</td>
			<td>4559086</td>
			<td>2DB01</td>
            <td>Depok</td>
		</tr>
		<tr>
			<td>5</td>
			<td>Jocelyn Firtus</td>
			<td>3355657</td>
			<td>2DB02</td>
            <td>Surabaya</td>
		</tr>
	</table>

<h2 style="color:black;font-family:arial">Form Mahasiswa</h2>
<table border="1" style="background:white">
<form method="post" action="">
    <tr><td>Nama</td><td><input type="text" name="nama" style="width:300px"></td></tr>
    <tr><td>NPM</td><td><input type="text" name="npm" style="width:300px"></td></tr>
    <tr><td>Kelas</td><td><input type="text" name="kls" style="width:300px"></td></tr>
    <tr><td>Alamat</td><td><textarea name="data" style="width:300px"></textarea></td></tr>
    </table>
    
    <tr><td></td><td><input type="submit" name="ok" value="Simpan"></td></tr>
    
</form>
</body>
</html>

<?php
if(isset($_POST['ok'])){
    if(empty($_POST['nama']) || empty($_POST['data'])){
        print "Lengkapi form";
    }else{
        $nama=$_POST['nama'];
        $npm=$_POST['npm'];
        $kls=$_POST['kls'];
        $data=$_POST['data'];
        $tanggal=date("h:i:s");
        $buka=fopen("hasil.html",'a');
        fwrite($buka,"nama : ${nama} <br> ");
        fwrite($buka,"npm : ${npm} <br> ");
        fwrite($buka,"kelas : ${kls} <br> ");
        fwrite($buka,"dibuat : ${tanggal} <br> ");
        fwrite($buka,"alamat : ${data} <br> ");
        fwrite($buka,"<hr>");
        fclose($buka);
        print "data berhasil disimpan";
    }
}?>